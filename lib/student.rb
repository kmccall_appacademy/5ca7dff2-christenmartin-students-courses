class Student
  attr_reader :first_name, :last_name
  attr_accessor :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    @first_name + " " + @last_name
  end

  def enroll(course)
    conflicts = @courses.any? {|existing_course| course.conflicts_with?(existing_course)}
    raise 'Error: new course conflicts with an existing course!' if conflicts

    @courses << course unless @courses.include?(course)
    course.students << self unless course.students.include?(self)
  end


  def course_load
    all = Hash.new(0)
    @courses.each {|course| all[course.department] += course.credits}
    all
  end

end
